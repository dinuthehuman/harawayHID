# harawayHID
harawayHID by Konglomerat M.I.D.I.

---
## Cyborg Dependencies installieren

#### NOOBS Bloatware rausschmeissen:
    sudo apt-get purge wolfram-engine scratch minecraft-pi libreoffice* claws-mail bluej bluez greenfoot sonic-pi idle idle3 dillo
    sudo apt-get autoremove
    sudo apt-get clean
    
#### div. Dependencies installieren
    sudo apt-get install git geany nodejs npm

#### Chromium unter Jessie:
    wget -qO - http://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
    echo "deb http://dl.bintray.com/kusti8/chromium-rpi jessie main" | sudo tee -a /etc/apt/sources.list
    sudo apt-get update
    sudo apt-get install chromium-browser
Quelle: 
[https://www.raspberrypi.org/forums/viewtopic.php?t=121195](https://www.raspberrypi.org/forums/viewtopic.php?t=121195)

####NodeJS packages installieren

    cd ~/harawayHID
    sudo npm install -g npm@latest
    npm init
    npm install express --save
    sudo npm install rpio --save
    chown -R pi:pi .
    
Quelle: 
[http://expressjs.com/en/starter/installing.html](http://expressjs.com/en/starter/installing.html)
 | 
Quelle: 
[https://github.com/jperkin/node-rpio](https://github.com/jperkin/node-rpio)

