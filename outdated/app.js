/************************
 * Module importieren
*************************/

//Express importieren
module.exports = require('./node_modules/express/lib/express');
var express = require('express');
var app = express();

//GPIO importieren
var gpio = require('pi-gpio');


/************************
 * Express-Events
*************************/

//GPIO-Calls handhaben
app.get('/gpio/:gpInt', function (req, res) {
	gpioShift(req.params.gpInt);
	if(debug) res.send('Hello World!<br />'+req.params.gpInt);
	else res.send();
});

//HTML/RTC-Files servieren
app.use('/rtc', express.static('./rtc'));

//Server starten
app.listen(3000, function () {
	console.log('express listening on port 3000!');
});


/************************
 * Global Scope & Setup
*************************/

//Debugging-Modus
var debug = false;

//GPIO-Array  X  A  B  Y  <  >  ^  v
var gpList = [29,31,32,33,35,36,37,38];

var gpLength = gpList.length;

//GPIOs starten
gpioSetup();
//gpN = 0;
delayedSetup();

console.log('GPIO set up');


/************************
 * GPIO-Funktionen
*************************/

//GPIO Setup
function gpioSetup() {
	for(n = 0; n < gpLength; n++) {
		gpio.open(gpList[n], "output",function(err){if(debug) console.log(err);});
	}
}

//Verzögertes GIPO-Setup
function delayedSetup() {
	gpio.open(gpList[gpN], "output",function(err){if(debug) console.log(err);});
	console.log("GIPO Pin " + gpList[gpN] + " set-up");
	gpN++;
	if( gpN < gpLength ){
		setTimeout( execSetup, 500 );
	}
}

//GPIOs ein- und ausschalten
function gpioShift(gpInt) {
	thisBit = 0;
	for (n = 0; n < gpLength; n++){
		thisBit = (gpInt & Math.pow(2,n))/Math.pow(2,n);
		gpio.write(gpList[n], thisBit, function(err){if(debug) console.log(err);});
	}
}
