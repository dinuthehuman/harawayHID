/************************
 * Module importieren
*************************/

//Express importieren
module.exports = require('./node_modules/express/lib/express');
var express = require('express');
var app = express();

//GPIO-Lib importieren
var rpio = require('rpio');

//ChildProcess (shell commands) importieren
const exec = require('child_process').exec;


/************************
 * Express-Events
*************************/

//GPIO-Calls handhaben
app.get('/gpio/:gpInt', function (req, res) {
	gpioShift(req.params.gpInt);
	res.send();
});

//HTML/RTC-Files servieren
app.use('/rtc', express.static('./rtc'));

//Shell-Commands handhaben
app.get('/sys/:cmd', function (req, res) {
	//Bash-Befehl definieren
	if(req.params.cmd == 'shutdown') cmdString = 'sudo shutdown -h now';
	if(req.params.cmd == 'reboot') cmdString = 'sudo reboot';
	//Befehl ausführen
	exec(cmdString, function(error, stdout, stderr) {
		console.log('stdout: ' + stdout);
		console.log('stderr: ' + stderr);
		if (error !== null) {
			console.log('exec error: ' + error);
		}
	});
	//Senden
	res.send();
});

//Bildschirm-Beleuchtung ein- und ausschalten
app.get('/bgl/:param', function (req, res) {
	//relevanter Pin: GPIO18/phys12
	bglPin = 12;
	rpio.write(bglPin, req.params.param);
	//Senden
	res.send();
});

//Server starten
app.listen(3000, function () {
	console.log('express online, listening on port 3000');
});


/************************
 * Global Scope & Setup
*************************/

//Debugging-Modus
var debug = false;

//Pin-Array   X  A  B  Y  <  >  ^  v
var gpList = [29,31,32,33,35,36,37,38];

var gpLength = gpList.length;

//GPIOs starten
gpioSetup();
console.log('GPIO set up');


/************************
 * GPIO-Funktionen
*************************/

//GPIO Setup
function gpioSetup() {
	for(n = 0; n < gpLength; n++) {
		rpio.open(gpList[n], rpio.OUTPUT, rpio.LOW);
	}
}

//GPIOs ein- und ausschalten
function gpioShift(gpInt) {
	thisBit = 0;
	for (n = 0; n < gpLength; n++){
		thisBit = (gpInt & Math.pow(2,n))/Math.pow(2,n);
		rpio.write(gpList[n], thisBit);
	}
}
