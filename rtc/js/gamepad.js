var hasGP = false;
var repGP;

function gpTransmit(gpInt) {
        console.log("Transmitted: "+gpInt);
        if(typeof(window.activeConn) != "undefined") window.activeConn.send(gpInt);
}

function canGame() {
        return "getGamepads" in navigator;
}

function reportOnGamepad() {
        var gp = navigator.getGamepads()[0];
        var html = "";
        html += "id: "+gp.id+"<br/>";
        
         //DINU: acht Buttons als Int
        var gpInt = 0;
        
        for(var i=0;i<gp.buttons.length;i++) {
                html+= "Button "+(i+1)+": ";
                if(gp.buttons[i].pressed) html+= " pressed";
                html+= "<br/>";
        }
        
        for(var i = 0; i<4; i++) {
            if(gp.buttons[i].pressed) gpInt += Math.pow(2,i); 
        }
        
        /*
        for(var i=0;i<gp.axes.length; i+=2) {
                html+= "Stick "+(Math.ceil(i/2)+1)+": "+gp.axes[i]+","+gp.axes[i+1]+"<br/>";
                //html+= "Stick "+Math.ceil(i)+": "+gp.axes[i]+","+gp.axes[i+1]+"<br/>";
        }
        */
        for(var i = 0; i<gp.axes.length; i++) {
                html+= "Achse "+i+": "+gp.axes[i]+"<br/>";
                if (i==0) {
                        if (gp.axes[i] == -1) gpInt += Math.pow(2,4);
                        if (gp.axes[i] == 1) gpInt += Math.pow(2,5);
                }
                if (i==1) {
                        if (gp.axes[i] == -1) gpInt += Math.pow(2,6);
                        if (gp.axes[i] == 1) gpInt += Math.pow(2,7);
                }
        }
        document.getElementById("gamepadInt").innerHTML = gpInt;
        gpTransmit(gpInt);
        //ENDE
        
        //$("#gamepadDisplay").html(html);
}
        
$(document).ready(function() {

        if(canGame()) {

                //var prompt = "To begin using your gamepad, connect it and press any button!";
                //$("#gamepadPrompt").text(prompt);
                
                $(window).on("gamepadconnected", function() {
                        hasGP = true;
                        //$("#gamepadPrompt").html("Gamepad connected!");
                        console.log("connection event");
                        repGP = window.setInterval(reportOnGamepad,100);
                });

                $(window).on("gamepaddisconnected", function() {
                        console.log("disconnection event");
                        //$("#gamepadPrompt").text(prompt);
                        window.clearInterval(repGP);
                });

                //setup an interval for Chrome
                var checkGP = window.setInterval(function() {
                        console.log('checkGP');
                        if(navigator.getGamepads()[0]) {
                                if(!hasGP) $(window).trigger("gamepadconnected");
                                window.clearInterval(checkGP);
                        }
                }, 500);
        }
        
});