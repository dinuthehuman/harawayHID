/****************************
 * Global Scope: Peer-Objekt
*****************************/

// Compatibility shim
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

//PeerJS-API-Key
var peerKey = 'kwm5oyhpunkd42t9';

//PeerJS-Debug-Level
var peerDebug = 3;

//Peer-Objekt öffnen -> URL GET Parameter 'id', eigene Turn- und Stun-Server
var peer = new Peer(
    GetURLParameter('id'), {
        key: peerKey,
        debug: peerDebug,
        config: {
            'iceServers': [
                { url: 'stun:46.101.125.1:3478'},
                //{ url: 'turn:46.101.125.1:3478'},
                { url: 'turn:46.101.125.1:3478', username:"cyborg3", credential: 'harawayHID'}
            ]
        }
    }
);

//Ist DataChannel aufgebaut -> Gamepad Workaround
gpActive = false;

/************************
 * Medien-Funktionen
*************************/

//Media-Stream abfangen
function medien(mitVideo) {
    // Get audio/video stream
    navigator.getUserMedia({audio: true, video: mitVideo}, function(stream){
        // Set your video displays
        $('#myVideo').prop('src', URL.createObjectURL(stream));
        window.localStream = stream;
        //UI-Zeugs
        dispDial();
    }, function(){ $('#mediaAccessError').show(); });
}
    

/************************
 * Peer-Funktionen
*************************/

function anrufErhalten(call) {
    // Hang up on an existing call if present
    if (window.existingCall) {
        window.existingCall.close();
    }

    // Wait for stream on the call, then set peer video display
    call.on('stream', function(stream){
        $('#remoteVideo').prop('src', URL.createObjectURL(stream));
    });
      
    //Auf Mitteilung hören
    peer.on('connection', function(conn) {
	conn.on('data', function(data){
            nodeUrl = "../gpio/" + data;
	    // erhaltene Mitteilung in DIV schreiben
	    document.getElementById("msg").innerHTML = data;
            //Nachricht an Express senden
            $.ajax({url: nodeUrl, type:"GET"});
	});
    });
    
    // UI stuff
    window.existingCall = call;
    dispCall();
    
    //Verbindung geschlossen, Komm. abbrechen
    call.on('close', verbindungAbbrechen);
    
}


function chatVerbinden(connectID) {
    //DataChannel aufbauen
    conn = peer.connect(connectID);
    conn.on('open', function(){
        conn.send('0');
        window.activeConn = conn;
    });
}


function verbindungAbbrechen() {
    if (window.existingCall) {
        window.existingCall.close();
    }
    if (window.activeConn) {
        window.activeConn.send('0');
        window.activeConn.close();
    }
    dispDial();
}


/************************
 * Event Handler: Buttons
*************************/

$(function(){
    $('#makeCall').click(function(){
	
	//ID zwischenspeichern
    //AENDERUNG SMALL SCREEN INTERFACE
	var idToCall = $('input[name="calltoID"]:checked').val();
        //alert(idToCall);
        // Initiate a call!
        var call = peer.call(idToCall, window.localStream);
	
        //DataChannel aufbauen
        chatVerbinden(idToCall);
	//Verbindung am Leben erhalten
        anrufErhalten(call);
    });

    $('#endCall').click(function(){
        verbindungAbbrechen();
    });

    // Retry if getUserMedia fails
    $('#mediaRetry').click(function(){
        $('#mediaAccessError').hide();
        medien();
    });
    
    $('#logOut').click(function(){
        if (window.existingCall) {
            window.existingCall.close();
        }
        window.location.href = "roleSelector.html";
    });

});


/************************
 * Event Handler: Peer
*************************/

//Peer geöffnet, eigene ID ausgeben
peer.on('open', function(){
    $('#myID').text(peer.id);
});

// Erhaltenen Anruf beantworten
peer.on('call', function(call){
    //Direkt antworten
    call.answer(window.localStream);
    
    //DataChannel aufbauen
    chatVerbinden(call.peer);
    //Verbindung am Leben erhalten
    anrufErhalten(call);
});

//Verbindungsfehler
peer.on('error', function(err){
    alert(err.message);
    // Return to step 2 if error occurs
    dispDial();
});


/************************
 * UI-Funktionen
*************************/

//"Wählscheibe" anzeigen
function dispDial () {
    $('#mediaError').hide();
    $('#aktAnruf').hide();
    $('#remoteID').text('no one');
    $('#wahlscheibe').show();
}

//aktuellen Anruf anzeigen
function dispCall() {
    $('#remoteID').text(window.existingCall.peer);
    $('#mediaError').hide();
    $('#wahlscheibe').hide();
    $('#aktAnruf').show();
}